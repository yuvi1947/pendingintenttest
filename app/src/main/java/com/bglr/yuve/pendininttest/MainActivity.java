package com.bglr.yuve.pendininttest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();

            //THE BELOW PART IS ALWAYS EMPTY

            String test = intent.getStringExtra("rcBookingControl");
            String ctrl = null;
            if (bundle != null)
                ctrl = bundle.getString("rcBookingControl");

            if (ctrl != null)
                Log.d(TAG, ctrl);

        }

        //DISPLAY NOTIFICATION
        new NotificationBaseClass().createTestNotification(getApplicationContext(), "SuperTatkal Pro", "Booking in Progress...");

    }
}
