package com.bglr.yuve.pendininttest;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

public class NotificationBaseClass {

    NotificationManager notificationManager;
    String ADMIN_CHANNEL_ID = "admin_channel";
    String notifications_admin_channel_name = "admin_channel";
    String notifications_admin_channel_description = "This channel is used to send priority messages";
    String TAG = this.getClass().getSimpleName();

    public void createTestNotification(Context context, String title, String msg) {

        boolean isSound = true;

        int notificationId = 0;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //setting up builder
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(msg)
                .setAutoCancel(true);

        //Setting up Notification channels for android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels();
            notificationBuilder.setChannelId(ADMIN_CHANNEL_ID);
        }

        //add intent to open the app
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder.setContentIntent(pendingIntent);

        //set option to not cancel it
        notificationBuilder.setAutoCancel(false);
        notificationBuilder.setOngoing(true);

        //adding custom action buttons
        Intent stopBookingIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        intent.putExtra("rcBookingControl", "stopBooking");
        stopBookingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);

        intent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent stopBookingPendingIntent = PendingIntent.getActivity(context.getApplicationContext(),
                (int) (Math.random() * 100), stopBookingIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(R.mipmap.ic_launcher, "STOP BOOKING", stopBookingPendingIntent);

        //adding sound
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (isSound) {
            notificationBuilder.setSound(defaultSoundUri);
            //some times default sound will be disabled
//            MyUtil.playNotificationSound(context);
        }

        //display notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = notifications_admin_channel_name;
        String adminChannelDescription = notifications_admin_channel_description;

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }


}
